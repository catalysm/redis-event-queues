import { Publisher } from "../lib/publisher"
import { Subscriber } from "../lib/subscriber"
import { expect } from 'chai'
import 'mocha'
import { createClient, RedisClient } from "redis";

let subClient: RedisClient
let pubClient: RedisClient

let publisher: Publisher
let subscriber: Subscriber

beforeEach(() => {
    subClient = createClient();
    pubClient = createClient();

    publisher = new Publisher({ name: 'test', client: pubClient });
    subscriber = new Subscriber({ name: 'test', client: subClient });
})

afterEach(() => {
    pubClient.del(publisher.eventList)
    pubClient.del(publisher.eventStore)
    publisher.destroy();
    subscriber.destroy();
})

describe('Pub Sub', () => {
   it('should send and receive basic string messages', (done) => {
        publisher.publish('testmessage');

        subscriber.on('data', (data: any) => {
            expect(data).to.eq('testmessage');
            done();
        });
    });

    it('should send and receive numbers', (done) => {
        publisher.publish(5);

        subscriber.on('data', (data: any) => {
            expect(data).to.eq(5);
            done();
        })

    })

    it('should send and receive complex data objects', (done) => {
        publisher.publish({ firstMessage: "first", secondMessage: "second" });

        subscriber.on('data', data => {
            expect(data).to.have.property('firstMessage');
            expect(data).to.have.property('secondMessage');
            expect(data.firstMessage).to.eq('first');
            expect(data.secondMessage).to.eq('second')
            done();
        });
    });

});



