import * as redis from 'redis';
import { promisify } from 'util';
import { EventEmitter } from 'events';

export interface SubscriberOptions {
    name: string,
    client: redis.RedisClient
}

export class Subscriber extends EventEmitter {
    constructor(options: SubscriberOptions) {
        super();

        if (options.name === undefined) {
            throw new Error('Must provide a name in subscriber options')
        }

        if (options.client === undefined) {
            throw new Error('Must provide a redis client in subscriber options')
        }

        this.client = options.client;
        this.clientSub = this.client.duplicate();
        this.name = options.name;
        this.eventStore = this.name + '_events';
        this.eventList = this.name + '_list';


        this.hgetAsync = promisify(this.client.hget).bind(this.client);
        this.hdelAsync = promisify(this.client.hdel).bind(this.client);
        this.rpopAsync = promisify(this.client.rpop).bind(this.client);

        this.init();
        return
    }



    public init() {
        this.clientSub.subscribe(this.name);

        this.clientSub.on('message', (channel, message) => {
            this.handleNewEvent(message)
        });
        return
    }

    public destroy() {
        this.client.quit();
        this.clientSub.quit();
        return
    }

    private async handleNewEvent(message: string) {
        let eventId: string = await this.rpopAsync(this.eventList);

        if (eventId !== null) {

            let eventData: any = await this.hgetAsync(this.eventStore, eventId);

            if (eventData !== null) {
                
                this.emit('data', JSON.parse(eventData));
            }

        }

    }

    name: string;
    eventStore: string;
    eventList: string;
    client: redis.RedisClient;
    private clientSub: redis.RedisClient;

    private hgetAsync: Function;
    private hdelAsync: Function;
    private rpopAsync: Function;
}
