import * as redis from 'redis';
import { promisify } from 'util';
import {v4 as uuid} from 'uuid';

export interface PublisherOptions {
    name: string,
    client: redis.RedisClient
}

export class Publisher {
    constructor(options : PublisherOptions) {
        if (options.name === undefined) {
            throw new Error('Must provide a name in publisher options')
        }
        
        if (options.client === undefined) {
            throw new Error('Must provide a redis client in publisher options')
        }
        
        this.client = options.client;
        this.name = options.name;
        this.eventStore = this.name + '_events';
        this.eventList = this.name + '_list';

        this.publishAsync = promisify(this.client.publish).bind(this.client);
        this.lpushAsync = promisify(this.client.lpush).bind(this.client);
        this.hsetAsync = promisify(this.client.hset).bind(this.client);
        
    }

    async publish(event : any) {
        let eventId : string = uuid();
        await this.lpushAsync(this.eventList, eventId);
        await this.hsetAsync(this.eventStore, eventId, JSON.stringify(event))
        await this.publishAsync(this.name, eventId);
    }

    destroy() {
        this.client.quit();
    }



    name: string;
    eventStore: string;
    eventList: string;
    client: redis.RedisClient;
    private publishAsync : Function;
    private lpushAsync: Function;
    private hsetAsync: Function;
}
