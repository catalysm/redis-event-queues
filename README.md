[![Build Status](https://travis-ci.org/CatalysmsServerManager/Redis-event-queues.svg?branch=master)](https://travis-ci.org/CatalysmsServerManager/Redis-event-queues)

# Redis event queues

[Docs](https://catalysmsservermanager.github.io/Redis-event-queues/)


Extends on Redis' default pub/sub mechanism.

